import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)},
  { path: 'programa-nacional', loadChildren: () => import('./pages/programa-nacional/programa-nacional.module').then( m => m.ProgramaNacionalPageModule)},
  { path: 'sociedade', loadChildren: () => import('./pages/sociedade/sociedade.module').then( m => m.SociedadePageModule)},
  { path: 'vacinacao-prematuros', loadChildren: () => import('./pages/vacinacao-prematuros/vacinacao-prematuros.module').then( m => m.VacinacaoPrematurosPageModule)},
  { path: 'referencias', loadChildren: () => import('./pages/referencias/referencias.module').then( m => m.ReferenciasPageModule)},
  { path: 'autores', loadChildren: () => import('./pages/autores/autores.module').then( m => m.AutoresPageModule)},
  { path: 'cadastro', loadChildren: () => import('./pages/cadastro/cadastro.module').then( m => m.CadastroPageModule)},
  { path: 'cadastroca', loadChildren: () => import('./pages/cadastroCA/cadastroca.module').then( m => m.CadastroCaPageModule)},
  { path: 'covid', loadChildren: () => import('./pages/covid/covid.module').then( m => m.CovidPageModule)},
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)},
  { path: 'informacao-vacina', loadChildren: () => import('./pages/informacao-vacina/informacao-vacina.module').then( m => m.InformacaoVacinaPageModule)},
  { path: 'comparacao-vacina', loadChildren: () => import('./pages/comparacao-vacina/comparacao-vacina.module').then( m => m.ComparacaoVacinaPageModule)},
  { path: 'signup', loadChildren: () => import('./pages/signup/signup.module').then( m => m.SignupPageModule)},
  { path: 'edit-user', loadChildren: () => import('./pages/edit-user/edit-user.module').then( m => m.EditUserPageModule)},
  { path: 'edit-rn', loadChildren: () => import('./pages/edit-rn/edit-rn.module').then( m => m.EditRnPageModule)},
  { path: 'vacina-tomar', loadChildren: () => import('./pages/vacinasTomar/vacina-tomar.module').then( m => m.VacinaTomarPageModule)},
  { path: 'caderneta', loadChildren: () => import('./pages/cadernetaVacinacao/cadernetaVacinacao.module').then( m => m.CadernetaVacinacaoPageModule)},
  { path: 'info', loadChildren: () => import('./pages/info/info.module').then( m => m.InfoPageModule)},
  { path: 'cadernetaVac', loadChildren: ()=> import('./pages/caderneta/caderneta.module').then(m => m.CadernetaPageModule)},
  { path: 'comp', loadChildren:()=> import('./pages/comp/comp.module').then(m => m.CompPageModule)}
  

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
