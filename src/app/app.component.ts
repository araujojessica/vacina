import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, NavController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TokenService } from './core/token/token.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public menuCtrl: MenuController,
    private navCtrl: NavController,
    public router: Router,
    private token: TokenService
  ) {

    this.initializeApp();
    if(token.getToken() != null){
      this.navCtrl.navigateForward('/home');
    }else {
      this.navCtrl.navigateForward('');
    }

  }

  initializeApp(){
    this.platform.ready().then ( () => {
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(true);
      this.statusBar.backgroundColorByHexString('#00FFFFFF');
      this.splashScreen.hide();
    });
  }

  toHome(){
    this.router.navigate(['/home']);
    this.menuCtrl.close();
  }

  toPrematuros(){
    this.router.navigate(['/vacinacao-prematuros']);
    this.menuCtrl.close();
  }

  toPrograma(){
    this.router.navigate(['/programa-nacional']);
    this.menuCtrl.close();
  }

  toSociedade(){
    this.router.navigate(['/sociedade']);
    this.menuCtrl.close();
  }

  toCovid(){
    this.router.navigate(['/covid']);
    this.menuCtrl.close();
  }

  toEditRn(){
    this.router.navigate(['/edit-rn']);
    this.menuCtrl.close();
  }

  toEditUser(){
    this.router.navigate(['/edit-user']);
    this.menuCtrl.close();
  }

  toAutores(){
    this.router.navigate(['/autores']);
    this.menuCtrl.close();
  }

  toReferencias(){
    this.router.navigate(['/referencias']);
    this.menuCtrl.close();
  }

  cadastro(){
    this.router.navigate(['/cadastroca']);
    this.menuCtrl.close();
  }
  caderneta(){
    this.router.navigate(['/caderneta']);
    this.menuCtrl.close();
  }
  info(){
    this.router.navigate(['/info']);
    this.menuCtrl.close();
  }

  logout() {
    localStorage.clear();
    localStorage.removeItem('nome')
    this.router.navigate(['']);
    this.menuCtrl.close();
    //navigator['app'].exitApp();
   }

}