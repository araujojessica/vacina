import { Injectable } from "@angular/core";
import { AbstractControl } from "@angular/forms";
import {debounceTime, switchMap, map, first} from 'rxjs/operators';
import { RegisterService } from "./register";
@Injectable({providedIn: 'root' })
export class UserNotTakenValidatorService {

    constructor(private signUpService: RegisterService) {}
    
    checkUserNameTaken() {
        return (control: AbstractControl) => {
            return control.valueChanges.pipe(
                debounceTime(300)
            ).pipe(
                switchMap( sus => this.signUpService.checkUserNameTaken(sus))
            ).pipe(
                map( isTaken => isTaken ? {userNameTaken: true} : null)
            ).pipe(
                first()
            );
        };
    }

    checkEmailTaken() {
        return (control: AbstractControl) => {
            return control.valueChanges.pipe(
                debounceTime(300)
            ).pipe(
                switchMap( email => this.signUpService.checkEmailTaken(email))
            ).pipe(
                map( isTaken => isTaken ? {emailTaken: true} : null)
            ).pipe(
                first()
            );
        };
    }
}
