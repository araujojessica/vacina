import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "src/app/core/user/user";
import { NewUser } from "../../interfaces/newUser";
import { NewRN } from "src/app/interfaces/newRN";
import { connectableObservableDescriptor } from "rxjs/internal/observable/ConnectableObservable";
import { Vacina } from "src/app/interfaces/vacina";

const API_URL = "http://159.203.181.9:3005"; 
//const API_URL = 'http://localhost:3005';

@Injectable({providedIn: 'root'})
export class RegisterService{

constructor(private http: HttpClient){}
    checkUserNameTaken(userName: string) {
       return this.http.get(API_URL + '/user/exists/' + userName);
    } 

    checkEmailTaken(email: string) {
        return this.http.get(API_URL + '/user/existsEmail/' + email);
     } 

    signup(newUser : NewUser){
        return this.http.post(API_URL +  '/user/signup', newUser);
    }

    getAll() {
        return this.http.get<User[]>(`/users`);
    }

    getById(id: number) {
        return this.http.get( API_URL + `/users/` + id);
    }

    
    registerRN(newRN: NewRN) {
        return this.http.post(API_URL + '/user/registerRN/', newRN)
    }

    update(user: User) {
        return this.http.put(API_URL + `/users/` + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(API_URL  + `/users/` + id);
    }

    findUserUnico(userName){
        return this.http.get<any>(API_URL  + '/user/findUserUnico/'+userName);
    }
    findRN(responsavel) {
        return this.http.get<NewRN[]>(API_URL  + '/user/findRN/'+responsavel);
    }
    editUsuario(newuser) {
        return this.http.post(API_URL + '/user/editFullName/', newuser)
    }

    editUsuarioPassword(newuser) {
        return this.http.post(API_URL + '/user/editPassword/', newuser)
    }

    editUsuarioEmail(newuser) {
        console.log('new user' + newuser)
        return this.http.post(API_URL + '/user/editEmail/', newuser)
    }
    editSenha(newuser) {
        return this.http.post(API_URL + '/user/editPassword/', newuser)
    }
    findRNUnico(rn) {
        return this.http.get<NewRN>(API_URL  + '/user/findRNUnico/'+rn);
    }
    updateRN(nome,data,id){
        
        return this.http.get(API_URL  + '/user/editRN/'+nome+'/'+data+'/'+id+'');
    }
    deleteRN(id){
        return this.http.get(API_URL + '/user/deleteRN/'+id+'')
    }
    findVacina(idade){
        return this.http.get<Vacina[]>(API_URL + '/vacina/'+idade+'')

    }

    registerCheck(responsavel,rn,vacina,data){
        return this.http.get(API_URL + '/vacina/check/'+responsavel+'/'+rn+'/'+vacina+'/'+data+'')

    }
    findCheck(rn){
        return this.http.get<any[]>(API_URL + '/vacina/checkVacina/'+rn+'')

    }
    find(idade) {
        return this.http.get<any[]>(API_URL  + '/vacina/findVacina/'+idade+'');
    }
    caderneta(responsavel) {
        return this.http.get<any[]>(API_URL  + '/caderneta/'+responsavel+'');
    }

    deleteCheck(id) {
        return this.http.get(API_URL  + '/vacina/delete/'+id+'');
    }
}