import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { tap } from "rxjs/operators";

const API_URL = 'http://159.203.181.9:3005';

@Injectable({providedIn: 'root'})
export class RecuperarSenhaService{
    
    constructor(private http:HttpClient){}

    sendemail(email:string){
        console.log('dentro do ts : ' + email)
        return this.http.post(`${API_URL}/sendEmail`,{email})
    };
}
