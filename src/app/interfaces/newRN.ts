export interface NewRN{
    id: number,
    fullname : string,
    data_nascimento : string,
    gestacao: string,
    idade: string,
    responsavel:string,
    data_cadastro:Date
}