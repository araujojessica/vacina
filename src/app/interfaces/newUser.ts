export interface NewUser{
    id: number,
    userName : string,
    email:string,
    password: string;
    fullName : string,
    sus : string,
    data_nascimento:Date;
}