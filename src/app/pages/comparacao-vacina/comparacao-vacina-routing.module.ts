import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ComparacaoVacinaPage } from './comparacao-vacina.page';

const routes: Routes = [
  {
    path: '',
    component: ComparacaoVacinaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComparacaoVacinaPageRoutingModule {}
