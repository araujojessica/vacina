import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ComparacaoVacinaPageRoutingModule } from './comparacao-vacina-routing.module';

import { ComparacaoVacinaPage } from './comparacao-vacina.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComparacaoVacinaPageRoutingModule
  ],
  declarations: [ComparacaoVacinaPage]
})
export class ComparacaoVacinaPageModule {}
