import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-comparacao-vacina',
  templateUrl: './comparacao-vacina.page.html',
  styleUrls: ['./comparacao-vacina.page.scss'],
})
export class ComparacaoVacinaPage implements OnInit {

  constructor(
    public modalCtrl: ModalController,
    private menuCtrl: MenuController
  ) { }

  ngOnInit() {
  }

  dismiss(){
    this.modalCtrl.dismiss();
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

}
