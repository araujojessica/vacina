import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InformacaoVacinaPageRoutingModule } from './informacao-vacina-routing.module';

import { InformacaoVacinaPage } from './informacao-vacina.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InformacaoVacinaPageRoutingModule
  ],
  declarations: [InformacaoVacinaPage]
})
export class InformacaoVacinaPageModule {}
