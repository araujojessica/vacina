import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformacaoVacinaPage } from './informacao-vacina.page';

const routes: Routes = [
  {
    path: '',
    component: InformacaoVacinaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformacaoVacinaPageRoutingModule {}
