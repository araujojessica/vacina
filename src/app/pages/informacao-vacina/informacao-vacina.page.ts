import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController, ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-informacao-vacina',
  templateUrl: './informacao-vacina.page.html',
  styleUrls: ['./informacao-vacina.page.scss'],
})
export class InformacaoVacinaPage implements OnInit {

  tipo: any;

  constructor(
    public modalCtrl: ModalController,
    public params: NavParams,
    public route: ActivatedRoute,
    public menuCtrl: MenuController
  ) {

    this.tipo = this.params.get('vacina')

   }

  ngOnInit() {
    //console.log(this.tipo)
  }

  dismiss(){
    this.modalCtrl.dismiss();
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

}
