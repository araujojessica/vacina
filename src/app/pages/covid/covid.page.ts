import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController } from '@ionic/angular';
import { InformacaoVacinaPage } from '../informacao-vacina/informacao-vacina.page';

@Component({
  selector: 'app-covid',
  templateUrl: './covid.page.html',
  styleUrls: ['./covid.page.scss'],
})
export class CovidPage implements OnInit {

  constructor(
    public modalCtrl: ModalController,
    private menuCtrl: MenuController
  ) { }

  ngOnInit() {
  }

  async toVacinas(tipo: any){
    const modal = await this.modalCtrl.create({
      component: InformacaoVacinaPage,
      componentProps: {
        vacina: tipo
      }
    });
    return await modal.present();
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

}
