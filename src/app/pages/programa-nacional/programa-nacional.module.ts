import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProgramaNacionalPageRoutingModule } from './programa-nacional-routing.module';

import { ProgramaNacionalPage } from './programa-nacional.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
 

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    ProgramaNacionalPageRoutingModule
  ],
  declarations: [ProgramaNacionalPage]
})
export class ProgramaNacionalPageModule {}
