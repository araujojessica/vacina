import { Component, NgModule, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { element } from 'protractor';
import { CovidPage } from '../covid/covid.page';
import { InformacaoVacinaPage } from '../informacao-vacina/informacao-vacina.page';

@Component({
  selector: 'app-programa-nacional',
  templateUrl: './programa-nacional.page.html',
  styleUrls: ['./programa-nacional.page.scss'],
})
export class ProgramaNacionalPage implements OnInit {

  degreeStyle1; degreeStyle2; degreeStyle3; degreeStyle4; degreeStyle5; degreeStyle6;
  degreeStyle7; degreeStyle8; degreeStyle9; degreeStyle10; degreeStyle11; degreeStyle12;
  
  /*filterItem: string;

  idt: string = "1"

  vacinas = [
    {nome: 'BCG'},
    {nome: 'Hepatite B'},
    {nome: 'Rotavírus'},
    {nome: 'Tríplice Bacteriana de Células Inteiras Combinadas Com Hib e Hepatite B (DTPw-HB/Hib)'},
    {nome: 'Vacina Tríplice Bacteriana Acelular do Tipo Adulto com Poliomielite – dTpa-VIP'},
    {nome: 'Vacinas Pneumocócicas Conjugadas'},
    {nome: 'Vacina Meningocócica C Conjugada'},
    {nome: 'Vacina Gripe/Influenza'},
    {nome: 'Vacina Febre Amarela'},
    {nome: 'Vacina Tríplice Viral (Sarampo, Caxumba e Rubéola) - SRC'},
    {nome: 'Vacina Tríplice Bacteriana de Células Inteiras - DTPw'},
    {nome: 'Vacina Poliomielite'},
    {nome: 'Vacina Tetraviral (Sarampo, Caxumba, Rubéola e Varicela) - SRC-V'},
    {nome: 'Vacina Hepatite A'},
    {nome: 'Vacina Varicela'},
    {nome: 'Vacina HPV4'},
    {nome: 'Vacina Hepatite B'},
    {nome: 'Vacina Meningocócica Conjugada - ACWY'},
    {nome: 'Vacina Dupla Bacteriana do Tipo Adulto – dT'}
  ]*/

  constructor(
    public modalCtrl: ModalController
  ) { }

  ngOnInit() {
    
  }

  async toVacinas(tipo: any){
    const modal = await this.modalCtrl.create({
      component: InformacaoVacinaPage,
      componentProps: {
        vacina: tipo
      }
    });
    return await modal.present();
  }

  async covid(){
    const modal = await this.modalCtrl.create({
      component: CovidPage
      
    });
    return await modal.present();
  }

  toggle(id: any){
  switch (id) {
      case 16:
        document.getElementById("16").style.display == "none" ?  (document.getElementById("16").style.display = "block", this.degreeStyle1 = `rotate(180deg)`) : (document.getElementById("16").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
        (document.getElementById("17").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
        (document.getElementById("18").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("19").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("20").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("21").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("22").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("23").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("24").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("25").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("26").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("27").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
        break;
      
      case 17:
        document.getElementById("17").style.display == "none" ?  (document.getElementById("17").style.display = "block", this.degreeStyle2 = `rotate(180deg)`) : (document.getElementById("17").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
        (document.getElementById("16").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
        (document.getElementById("18").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("19").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("20").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("21").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("22").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("23").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("24").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("25").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("26").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("27").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
        break;

      case 18:
        document.getElementById("18").style.display == "none" ?  (document.getElementById("18").style.display = "block", this.degreeStyle3 = `rotate(180deg)`) : (document.getElementById("18").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("17").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
        (document.getElementById("16").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("19").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("20").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("21").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("22").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("23").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("24").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("25").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("26").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("27").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
        break;

      case 19:
        document.getElementById("19").style.display == "none" ?  (document.getElementById("19").style.display = "block", this.degreeStyle4 = `rotate(180deg)`) : (document.getElementById("19").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("17").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
        (document.getElementById("18").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("16").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("20").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("21").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("22").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("23").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("24").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("25").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("26").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("27").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
        break;

      case 20:
        document.getElementById("20").style.display == "none" ?  (document.getElementById("20").style.display = "block", this.degreeStyle5 = `rotate(180deg)`) : (document.getElementById("20").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("17").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
        (document.getElementById("18").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("19").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("16").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("21").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("22").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("23").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("24").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("25").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("26").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("27").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
        break;

      case 21:
        document.getElementById("21").style.display == "none" ?  (document.getElementById("21").style.display = "block", this.degreeStyle6 = `rotate(180deg)`) : (document.getElementById("21").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("17").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
        (document.getElementById("18").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("19").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("20").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("16").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("22").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("23").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("24").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("25").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("26").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("27").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
        break;

      case 22:
        document.getElementById("22").style.display == "none" ?  (document.getElementById("22").style.display = "block", this.degreeStyle7 = `rotate(180deg)`) : (document.getElementById("22").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("17").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
        (document.getElementById("18").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("19").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("20").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("21").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("16").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("23").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("24").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("25").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("26").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("27").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
        break;
          
      case 23:
          document.getElementById("23").style.display == "none" ?  (document.getElementById("23").style.display = "block", this.degreeStyle8 = `rotate(180deg)`) : (document.getElementById("23").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("17").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
        (document.getElementById("18").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("19").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("20").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("21").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("22").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("16").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("24").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("25").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("26").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("27").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          break; 
            
      case 24:
      document.getElementById("24").style.display == "none" ?  (document.getElementById("24").style.display = "block", this.degreeStyle9 = `rotate(180deg)`) : (document.getElementById("24").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
      (document.getElementById("17").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
      (document.getElementById("18").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
      (document.getElementById("19").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
      (document.getElementById("20").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
      (document.getElementById("21").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
      (document.getElementById("22").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
      (document.getElementById("23").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
      (document.getElementById("16").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
      (document.getElementById("25").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
      (document.getElementById("26").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
      (document.getElementById("27").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
      break; 
            
      case 25:
      document.getElementById("25").style.display == "none" ?  (document.getElementById("25").style.display = "block", this.degreeStyle10 = `rotate(180deg)`) : (document.getElementById("25").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
      (document.getElementById("17").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
        (document.getElementById("18").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("19").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("20").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("21").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("22").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("23").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("24").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("16").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("26").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("27").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
      break; 
            
      case 26:
      document.getElementById("26").style.display == "none" ?  (document.getElementById("26").style.display = "block", this.degreeStyle11 = `rotate(180deg)`) : (document.getElementById("26").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
      (document.getElementById("17").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
        (document.getElementById("18").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("19").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("20").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("21").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("22").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("23").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("24").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("25").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("16").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("27").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
      break; 
            
      case 27:
      document.getElementById("27").style.display == "none" ?  (document.getElementById("27").style.display = "block", this.degreeStyle12 = `rotate(180deg)`) : (document.getElementById("27").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
      (document.getElementById("17").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
        (document.getElementById("18").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("19").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("20").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("21").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("22").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("23").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("24").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("25").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("26").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("16").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
      break;
  }}
  
}
