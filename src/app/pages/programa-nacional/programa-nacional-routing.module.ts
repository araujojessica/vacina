import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProgramaNacionalPage } from './programa-nacional.page';

const routes: Routes = [
  {
    path: '',
    component: ProgramaNacionalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProgramaNacionalPageRoutingModule {}
