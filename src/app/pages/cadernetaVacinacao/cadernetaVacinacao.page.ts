import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { NewRN } from 'src/app/interfaces/newRN';
import { RegisterService } from 'src/app/services/register/register';
import { CadernetaPage } from '../caderneta/caderneta.page';

@Component({
  selector: 'app-cadernetaVacinacao',
  templateUrl: './cadernetaVacinacao.page.html',
  styleUrls: ['./cadernetaVacinacao.page.scss'],
})
export class CadernetaVacinacaoPage implements OnInit {
  user$: Observable<User>;
  nome: string
  idResponsavel:any;
  rn:NewRN[];
  caderneta: any[];
  constructor(
    public modalCtrl: ModalController,
    private registerService:RegisterService,
    private userService: UserService,
    private menuCtrl: MenuController
  ) { 

    this.user$ = userService.getUser();
    this.user$.subscribe( usuario => {
     var n = usuario.fullName.split(' ');
     this.nome = n[0];  
     this.idResponsavel = usuario.id;   
    });
    this.registerService.findRN(this.idResponsavel).subscribe(rn => {
      this.rn = rn;
      
    })
  }

  ngOnInit() {
  }

  dismiss(){
    this.modalCtrl.dismiss();
  }
  async cad(rn,nome){
    const modal = await this.modalCtrl.create({
      component: CadernetaPage,
      componentProps: {
        rn: rn,
        nome:nome
      }
    });
    return await modal.present();
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

}
