import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadernetaVacinacaoPageRoutingModule } from './cadernetaVacinacao-routing.module';

import { CadernetaVacinacaoPage } from './cadernetaVacinacao.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadernetaVacinacaoPageRoutingModule
  ],
  declarations: [CadernetaVacinacaoPage]
})
export class CadernetaVacinacaoPageModule {}
