import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CadernetaVacinacaoPage } from './cadernetaVacinacao.page';

describe('CadernetaVacinacaoPage', () => {
  let component: CadernetaVacinacaoPage;
  let fixture: ComponentFixture<CadernetaVacinacaoPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CadernetaVacinacaoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CadernetaVacinacaoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
