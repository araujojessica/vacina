import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController } from '@ionic/angular';
import { CovidPage } from '../covid/covid.page';
import { InformacaoVacinaPage } from '../informacao-vacina/informacao-vacina.page';

@Component({
  selector: 'app-sociedade',
  templateUrl: './sociedade.page.html',
  styleUrls: ['./sociedade.page.scss'],
})
export class SociedadePage implements OnInit {

  degreeStyle1; degreeStyle2; degreeStyle3; degreeStyle4; degreeStyle5; degreeStyle6;
  degreeStyle7; degreeStyle8; degreeStyle9; degreeStyle10; degreeStyle11; degreeStyle12; degreeStyle13;

  constructor(
    public modalCtrl: ModalController,
    private menuCtrl: MenuController
  ) { }

  ngOnInit() {
  }

  async toVacinas(tipo: any){
    const modal = await this.modalCtrl.create({
      component: InformacaoVacinaPage,
      componentProps: {
        vacina: tipo
      }
    });
    return await modal.present();
  }

  async covid(){
    const modal = await this.modalCtrl.create({
      component: CovidPage
      
    });
    return await modal.present();
  }


  toggle(id){
    switch (id) {
        case 28:
          document.getElementById("28").style.display == "none" ?  (document.getElementById("28").style.display = "block", this.degreeStyle1 = `rotate(180deg)`) : (document.getElementById("28").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
          (document.getElementById("29").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
          (document.getElementById("30").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("31").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("32").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("33").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("34").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("35").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("36").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("37").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("38").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("39").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("40").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          break;
        
        case 29:
          document.getElementById("29").style.display == "none" ?  (document.getElementById("29").style.display = "block", this.degreeStyle2 = `rotate(180deg)`) : (document.getElementById("29").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
          (document.getElementById("28").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
          (document.getElementById("30").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("31").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("32").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("33").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("34").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("35").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("36").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("37").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("38").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("39").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("40").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          break;
  
        case 30:
          document.getElementById("30").style.display == "none" ?  (document.getElementById("30").style.display = "block", this.degreeStyle3 = `rotate(180deg)`) : (document.getElementById("30").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("29").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
          (document.getElementById("28").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("31").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("32").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("33").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("34").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("35").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("36").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("37").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("38").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("39").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("40").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          break;
  
        case 31:
          document.getElementById("31").style.display == "none" ?  (document.getElementById("31").style.display = "block", this.degreeStyle4 = `rotate(180deg)`) : (document.getElementById("31").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("29").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
          (document.getElementById("30").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("28").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("32").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("33").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("34").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("35").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("36").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("37").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("38").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("39").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("40").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          break;
  
        case 32:
          document.getElementById("32").style.display == "none" ?  (document.getElementById("32").style.display = "block", this.degreeStyle5 = `rotate(180deg)`) : (document.getElementById("32").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("29").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
          (document.getElementById("30").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("31").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("28").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("33").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("34").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("35").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("36").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("37").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("38").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("39").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("40").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          break;
  
        case 33:
          document.getElementById("33").style.display == "none" ?  (document.getElementById("33").style.display = "block", this.degreeStyle6 = `rotate(180deg)`) : (document.getElementById("33").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("29").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
          (document.getElementById("30").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("31").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("32").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("28").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("34").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("35").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("36").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("37").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("38").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("39").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("40").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          break;
  
        case 34:
          document.getElementById("34").style.display == "none" ?  (document.getElementById("34").style.display = "block", this.degreeStyle7 = `rotate(180deg)`) : (document.getElementById("34").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("29").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
          (document.getElementById("30").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("31").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("32").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("33").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("28").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("35").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("36").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("37").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("38").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("39").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("40").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          break;
            
        case 35:
            document.getElementById("35").style.display == "none" ?  (document.getElementById("35").style.display = "block", this.degreeStyle8 = `rotate(180deg)`) : (document.getElementById("35").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
            (document.getElementById("29").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
          (document.getElementById("30").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("31").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("32").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("33").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("34").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("28").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("36").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("37").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("38").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("39").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("40").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
            break; 
              
        case 36:
        document.getElementById("36").style.display == "none" ?  (document.getElementById("36").style.display = "block", this.degreeStyle9 = `rotate(180deg)`) : (document.getElementById("36").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("29").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
          (document.getElementById("30").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("31").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("32").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("33").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("34").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("35").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("28").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("37").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("38").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("39").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("40").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
        break; 
              
        case 37:
        document.getElementById("37").style.display == "none" ?  (document.getElementById("37").style.display = "block", this.degreeStyle10 = `rotate(180deg)`) : (document.getElementById("37").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("29").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
          (document.getElementById("30").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("31").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("32").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("33").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("34").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("35").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("36").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("28").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("38").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("39").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("40").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
        break; 
              
        case 38:
        document.getElementById("38").style.display == "none" ?  (document.getElementById("38").style.display = "block", this.degreeStyle11 = `rotate(180deg)`) : (document.getElementById("38").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("29").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
          (document.getElementById("30").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("31").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("32").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("33").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("34").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("35").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("36").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("37").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("28").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("39").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("40").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
        break; 
              
        case 39:
        document.getElementById("39").style.display == "none" ?  (document.getElementById("39").style.display = "block", this.degreeStyle12 = `rotate(180deg)`) : (document.getElementById("39").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
        (document.getElementById("29").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
          (document.getElementById("30").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("31").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("32").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("33").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("34").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("35").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("36").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("37").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("38").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("28").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("40").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
        break;

        case 40:
          document.getElementById("40").style.display == "none" ?  (document.getElementById("40").style.display = "block", this.degreeStyle13 = `rotate(180deg)`) : (document.getElementById("40").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          (document.getElementById("29").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
          (document.getElementById("30").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("31").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("32").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("33").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("34").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("35").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("36").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("37").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("38").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("39").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("28").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          break;
    }}

}
