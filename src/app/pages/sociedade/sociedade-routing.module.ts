import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SociedadePage } from './sociedade.page';

const routes: Routes = [
  {
    path: '',
    component: SociedadePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SociedadePageRoutingModule {}
