import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VacinacaoPrematurosPage } from './vacinacao-prematuros.page';

const routes: Routes = [
  {
    path: '',
    component: VacinacaoPrematurosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VacinacaoPrematurosPageRoutingModule {}
