import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VacinacaoPrematurosPage } from './vacinacao-prematuros.page';

describe('VacinacaoPrematurosPage', () => {
  let component: VacinacaoPrematurosPage;
  let fixture: ComponentFixture<VacinacaoPrematurosPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VacinacaoPrematurosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VacinacaoPrematurosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
