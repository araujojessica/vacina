import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VacinacaoPrematurosPageRoutingModule } from './vacinacao-prematuros-routing.module';

import { VacinacaoPrematurosPage } from './vacinacao-prematuros.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VacinacaoPrematurosPageRoutingModule
  ],
  declarations: [VacinacaoPrematurosPage]
})
export class VacinacaoPrematurosPageModule {}
