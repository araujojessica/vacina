import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, ModalController } from '@ionic/angular';
import { InformacaoVacinaPage } from '../informacao-vacina/informacao-vacina.page';

@Component({
  selector: 'app-vacinacao-prematuros',
  templateUrl: './vacinacao-prematuros.page.html',
  styleUrls: ['./vacinacao-prematuros.page.scss'],
})
export class VacinacaoPrematurosPage implements OnInit {

  degreeStyle1; degreeStyle2; degreeStyle3; degreeStyle4; degreeStyle5; 
  degreeStyle6; degreeStyle7; degreeStyle8; degreeStyle9; degreeStyle10; 
  degreeStyle11; degreeStyle12; degreeStyle13; degreeStyle14; degreeStyle15;

  constructor(
    public modalCtrl: ModalController,
  
  ) { }

  ngOnInit() {
  }

  async toVacinas(tipo: any){
    const modal = await this.modalCtrl.create({
      component: InformacaoVacinaPage,
      componentProps: {
        vacina: tipo
      }
    });
    return await modal.present();
  }

  

  toggle(id){
    switch (id) {
        case 1:
          document.getElementById("1").style.display == "none" ?  (document.getElementById("1").style.display = "block", this.degreeStyle1 = `rotate(180deg)`) : (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
          (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`); 
          (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
          (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
          break;
        
        case 2:
          document.getElementById("2").style.display == "none" ?  (document.getElementById("2").style.display = "block", this.degreeStyle2 = `rotate(180deg)`) : (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
          (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
          (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
          (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
          break;
  
        case 3:
          document.getElementById("3").style.display == "none" ?  (document.getElementById("3").style.display = "block", this.degreeStyle3 = `rotate(180deg)`) : (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          
          (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
          (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
          (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
          (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
          break;
  
        case 4:
          document.getElementById("4").style.display == "none" ?  (document.getElementById("4").style.display = "block", this.degreeStyle4 = `rotate(180deg)`) : (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
          (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
          (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
          (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
          break;
  
        case 5:
          document.getElementById("5").style.display == "none" ?  (document.getElementById("5").style.display = "block", this.degreeStyle5 = `rotate(180deg)`) : (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
          (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
          (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
          (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
          break;
  
        case 6:
          document.getElementById("6").style.display == "none" ?  (document.getElementById("6").style.display = "block", this.degreeStyle6 = `rotate(180deg)`) : (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
          (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
          (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
          (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
          break;
  
        case 7:
          document.getElementById("7").style.display == "none" ?  (document.getElementById("7").style.display = "block", this.degreeStyle7 = `rotate(180deg)`) : (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
          (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
          (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
          (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
          break;
            
        case 8:
            document.getElementById("8").style.display == "none" ?  (document.getElementById("8").style.display = "block", this.degreeStyle8 = `rotate(180deg)`) : (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
            (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
            (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
            (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
            (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
            (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
            (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
            (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
            (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
            (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
            (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
            (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
            (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
            (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
            (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
            break; 
              
        case 9:
        document.getElementById("9").style.display == "none" ?  (document.getElementById("9").style.display = "block", this.degreeStyle9 = `rotate(180deg)`) : (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
        (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
        (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
        (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
        (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
          (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
        break; 
              
        case 10:
        document.getElementById("10").style.display == "none" ?  (document.getElementById("10").style.display = "block", this.degreeStyle10 = `rotate(180deg)`) : (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
        (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
        (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
        (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
        (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
        (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
        break; 
              
        case 11:
        document.getElementById("11").style.display == "none" ?  (document.getElementById("11").style.display = "block", this.degreeStyle11 = `rotate(180deg)`) : (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
        (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
        (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
        (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
        (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
        (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
        break; 
              
        case 12:
        document.getElementById("12").style.display == "none" ?  (document.getElementById("12").style.display = "block", this.degreeStyle12 = `rotate(180deg)`) : (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
        (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
        (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
        (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
        (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
        (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
        (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
        (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
        (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
        (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
        (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
        (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
        (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
        (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
        (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
        break;

        case 13:
          document.getElementById("13").style.display == "none" ?  (document.getElementById("13").style.display = "block", this.degreeStyle13 = `rotate(180deg)`) : (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
          (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
          (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
          (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
          break;

        case 14:
          document.getElementById("14").style.display == "none" ?  (document.getElementById("14").style.display = "block", this.degreeStyle14 = `rotate(180deg)`) : (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
          (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
          (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
          (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
        break;

        case 15:
          document.getElementById("15").style.display == "none" ?  (document.getElementById("15").style.display = "block", this.degreeStyle15 = `rotate(180deg)`) : (document.getElementById("15").style.display = "none", this.degreeStyle15 = `rotate(360deg)`);
          (document.getElementById("1").style.display = "none", this.degreeStyle1 = `rotate(360deg)`);
          (document.getElementById("2").style.display = "none", this.degreeStyle2 = `rotate(360deg)`);
          (document.getElementById("3").style.display = "none", this.degreeStyle3 = `rotate(360deg)`);
          (document.getElementById("4").style.display = "none", this.degreeStyle4 = `rotate(360deg)`);
          (document.getElementById("5").style.display = "none", this.degreeStyle5 = `rotate(360deg)`);
          (document.getElementById("6").style.display = "none", this.degreeStyle6 = `rotate(360deg)`);
          (document.getElementById("7").style.display = "none", this.degreeStyle7 = `rotate(360deg)`);
          (document.getElementById("8").style.display = "none", this.degreeStyle8 = `rotate(360deg)`);
          (document.getElementById("9").style.display = "none", this.degreeStyle9 = `rotate(360deg)`);
          (document.getElementById("10").style.display = "none", this.degreeStyle10 = `rotate(360deg)`);
          (document.getElementById("11").style.display = "none", this.degreeStyle11 = `rotate(360deg)`);
          (document.getElementById("12").style.display = "none", this.degreeStyle12 = `rotate(360deg)`);
          (document.getElementById("13").style.display = "none", this.degreeStyle13 = `rotate(360deg)`);
          (document.getElementById("14").style.display = "none", this.degreeStyle14 = `rotate(360deg)`);
        break;
    }}

  }
