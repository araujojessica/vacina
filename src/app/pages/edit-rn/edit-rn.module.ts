import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditRnPageRoutingModule } from './edit-rn-routing.module';

import { EditRnPage } from './edit-rn.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    EditRnPageRoutingModule
  ],
  declarations: [EditRnPage]
})
export class EditRnPageModule {}
