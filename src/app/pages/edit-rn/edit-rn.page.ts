import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController, ModalController, NavController, NavParams, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { NewRN } from 'src/app/interfaces/newRN';
import { RegisterService } from 'src/app/services/register/register';

@Component({
  selector: 'app-edit-rn',
  templateUrl: './edit-rn.page.html',
  styleUrls: ['./edit-rn.page.scss'],
})
export class EditRnPage implements OnInit {
  user$: Observable<User>;
  fullName: any;
  sus:any;
  nome:any;
  newNome:any;
  newData:any;
  username:any;
  idUsuario:any;
  rn: NewRN[];
  nomeRN:any;
  idRN: any;
  data_nascimento:any;
  rnUnico: NewRN;
  constructor(
    public modalCtrl: ModalController,
    //public params: NavParams,
    public route: ActivatedRoute,
    private registerService: RegisterService,
    private userService: UserService,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private menuCtrl:MenuController
  ) {

    this.user$ = userService.getUser();
    this.user$.subscribe( usuario => {
     this.fullName = usuario.fullName;
     var n = usuario.fullName.split(' ');
     this.nome = n[0];
     this.sus = usuario.sus;
     this.idUsuario = usuario.id;

    });
    this.registerService.findRN(this.idUsuario).subscribe( rn => {
      this.rn = rn;
    })
   }

  ngOnInit() {
    //console.log(this.tipo)
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  editar(id){
    document.getElementById("editar-form").style.display = "none";
    document.getElementById("form-editar").style.display = "block";
    this.registerService.findRNUnico(id).subscribe( rn => {
        this.rnUnico = rn;
     
        this.nomeRN = this.rnUnico.fullname;
        this.data_nascimento = rn.data_nascimento;
        this.idRN = rn.id;
    
    })
  }

  nomeR(nome:any){
    this.newNome = nome.target.value;
  }
  data(data:any){
    this.newData = data.target.value;
  }

  salvar(id){
    if((this.newNome && this.newData) == undefined){
      
      this.navCtrl.navigateRoot('/edit-rn')
      document.getElementById("editar-form").style.display = "block";
      document.getElementById("form-editar").style.display = "none";
    }
    if(this.newNome != undefined || this.newData != undefined){
       if (this.newNome == undefined){
         this.newNome = this.rnUnico.fullname;
       }
       if(this.newData == undefined){
         this.newData = this.rnUnico.data_nascimento;
       }

       this.registerService.updateRN(this.newNome, this.newData, id).subscribe(
        async () => {
          document.getElementById("editar-form").style.display = "block";
          document.getElementById("form-editar").style.display = "none";
          const toast = await (await this.toastCtrl.create({
                message: 'Criança/Adolescente editado com sucesso!',
                duration: 4000, position: 'middle',
                color: 'success'
              })).present();
  
       },  
        async err => {
          const toast = await (await this.toastCtrl.create({
            message: 'Ocorreu um erro, por favor contate nosso ADM!',
            duration: 4000, position: 'middle',
            color: 'danger'
          })).present();
      
        });
  
       

    }

    
  }

  excluir(id){

    this.registerService.deleteRN(id).subscribe(
      async () => {
        document.getElementById("editar-form").style.display = "block";
        document.getElementById("form-editar").style.display = "none";
        const toast = await (await this.toastCtrl.create({
              message: 'Criança/Adolescente deletado com sucesso!',
              duration: 4000, position: 'middle',
              color: 'success'
            })).present();

     },  
      async err => {
        const toast = await (await this.toastCtrl.create({
          message: 'Ocorreu um erro, por favor contate nosso ADM!',
          duration: 4000, position: 'middle',
          color: 'danger'
        })).present();
    
      });
  
    
  }

  dismiss(){
    this.modalCtrl.dismiss();
  }

}
