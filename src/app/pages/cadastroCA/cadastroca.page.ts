import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuController, ToastController } from '@ionic/angular';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { NewRN } from 'src/app/interfaces/newRN';
import { NewUser } from 'src/app/interfaces/newUser';
import { RegisterService } from 'src/app/services/register/register';

@Component({
  selector: 'app-cadastroca',
  templateUrl: './cadastroca.page.html',
  styleUrls: ['./cadastroca.page.scss'],
})
export class CadastroCaPage implements OnInit {

  data_nascimento: string;
  nomeRN:string;
  newRN: NewRN;
  nome:string;
  idResponsavel:any;
  user$: Observable<User>;
  idade:any;
  public Login: FormGroup;

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private registerService: RegisterService,
    private userService: UserService,
    private formBuilder: FormBuilder, private menuCtrl: MenuController) { 

    this.user$ = userService.getUser();
    this.user$.subscribe( usuario => {
      var n = usuario.fullName.split(' ');
      this.nome = n[0];
      this.idResponsavel = usuario.id;    
    });
    }

    ngOnInit() {
      this.Login = this.formBuilder.group({
        fullname: [null, Validators.compose([Validators.required])],
        data_nascimento: [null,[Validators.required]],
        gestacao: [null,[Validators.required]]
      });
    }

    ionViewWillEnter() {
      this.menuCtrl.enable(true);
    }
  
    async gotohome() {
        const newRN  = this.Login.getRawValue() as NewRN;
        newRN.responsavel = this.idResponsavel;

        var d = newRN.data_nascimento.split('T');         
         var df = d[0].split('-');
         var dn = df[2] + "/" + df[1] + "/" + df[0];
    
         var d2 = new Date();
        
        var diff = moment(d2,"DD/MM/YYYY").diff(moment(dn,"DD/MM/YYYY"));
        var dias = moment.duration(diff).asDays();
        var mes = moment.duration(diff).asMonths();
        var ano = moment.duration(diff).asYears();
        this.idade = dias, mes, ano;

        if(Math.trunc(ano) == 0){
          if (Math.trunc(mes) == 0){
            if (Math.trunc(dias) <= 1){
                newRN.idade = Math.trunc(dias) + ' dia';
            }else {
                newRN.idade = Math.trunc(dias) + ' dias';
            }
            
          }else {
            if(Math.trunc(mes) <= 1){
                newRN.idade = Math.trunc(mes) + ' mês';
            }else {
                newRN.idade = Math.trunc(mes) + ' meses';
            }
            
          }
          
        }else {
          if (Math.trunc(ano) <= 1){
            if(Math.trunc(mes) <= 1){
                newRN.idade = Math.trunc(ano) + ' ano e ' + Math.trunc(mes) + ' mês'; 
            }else {
                newRN.idade = Math.trunc(ano) + ' ano e ' + Math.trunc(mes) + ' meses';
            }

          }else {
            newRN.idade = Math.trunc(ano) + ' anos';
          }
         
        }
      
        this.registerService.registerRN(newRN).subscribe(
          
          async () => {
            
              const toast = await (await this.toastCtrl.create({
                    message: 'Cadastro Realizado com Sucesso',
                    duration: 4000, position: 'middle',
                    color: 'success'
                  })).present();
  
           },  
          async err => {
            console.log(err)
            const toast = await (await this.toastCtrl.create({
              message: 'Ocorreu um erro, por favor contate nosso ADM!',
              duration: 4000, position: 'middle',
              color: 'danger'
            })).present();
          }
        );
  
  }
  more(){
    this.data_nascimento = null;
    this.nomeRN = null;
  }
  sair(){
   this.router.navigate(['/home']);
  }

  atualizar(){
    this.Login.reset();
  }

}
