import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadastroCaPageRoutingModule } from './cadastroca-routing.module';

import { CadastroCaPage } from './cadastroca.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CadastroCaPageRoutingModule
  ],
  declarations: [CadastroCaPage]
})
export class CadastroCaPageModule {}
