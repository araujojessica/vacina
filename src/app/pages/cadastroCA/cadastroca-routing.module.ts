import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadastroCaPage } from './cadastroca.page';

const routes: Routes = [
  {
    path: '',
    component: CadastroCaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadastroCaPageRoutingModule {}
