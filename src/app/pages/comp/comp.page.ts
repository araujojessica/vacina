import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController, NavParams } from '@ionic/angular';
import { Console } from 'console';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { NewRN } from 'src/app/interfaces/newRN';
import { RegisterService } from 'src/app/services/register/register';

@Component({
  selector: 'app-comp',
  templateUrl: './comp.page.html',
  styleUrls: ['./comp.page.scss'],
})
export class CompPage implements OnInit {
  user$: Observable<User>;
  nome: string
  idResponsavel:any;
  rn:NewRN[];
  vacina:any;
  caderneta: any[];
  c:any[];
  constructor(
    public modalCtrl: ModalController,
    private registerService:RegisterService,
    private userService: UserService,
    private params: NavParams,
    private menuCtrl: MenuController
  ) { 
    this.vacina = this.params.get('nome');
    this.user$ = userService.getUser();
    this.user$.subscribe( usuario => {
     var n = usuario.fullName.split(' ');
     this.nome = n[0];  
     this.idResponsavel = usuario.id;   
    });
  }

  ngOnInit() {
  }

  dismiss(){
    this.modalCtrl.dismiss();
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }
 
}
