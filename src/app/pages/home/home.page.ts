import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ELocalNotificationTriggerUnit, LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { AlertController, MenuController, ModalController, Platform } from '@ionic/angular';
import * as moment from 'moment';
import polling from 'rx-polling';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { NewRN } from 'src/app/interfaces/newRN';
import { Vacina } from 'src/app/interfaces/vacina';
import { RegisterService } from 'src/app/services/register/register';
import { ComparacaoVacinaPage } from '../comparacao-vacina/comparacao-vacina.page';
import { VacinaTomarPage } from '../vacinasTomar/vacina-tomar.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  nome: string
  idResponsavel:any;
  schedule=[];
  DataArray: Array<string> = [];
  user$: Observable<User>;
  rn:NewRN[];
  idade:any;
  vacina: Vacina[];
  gestacao:any;

  constructor(
    public router: Router,
    public menuCtrl: MenuController,
    public plt:Platform,
    public localNotifications:LocalNotifications,
    public alertCrtl: AlertController,
    public modalCtrl: ModalController,
    private userService: UserService,
    private registerService: RegisterService

  ) {
   this.plt.ready().then(() => {

    

      this.localNotifications.on('click').subscribe( res =>{
        console.log('click : ' + res)
        //let msg = res.data ? res.data.data:'';
        //this.showAlert(res.title,res.text,msg)
      });
      this.localNotifications.on('trigger').subscribe( res => {
        console.log('trigger : ' + res)
        //let msg = res.data ? res.data.data:'';
        //this.showAlert(res.title,res.text,msg)
      });
      this.localNotifications.setDefaults({
        led: { color: '#FF00FF', on: 500, off: 500 },
        vibrate: true,
        sound: 'file://assets/music/not.mp3'
    });
    });

    this.user$ = userService.getUser();
    this.user$.subscribe( usuario => {
     var n = usuario.fullName.split(' ');
     this.nome = n[0];  
     this.idResponsavel = usuario.id;   
    });

    const options = { interval: 30000 };
    const r = this.registerService.findRN(this.idResponsavel);
      polling(r,options).subscribe(rn =>{
        this.rn = rn;
        this.rn.forEach( r =>{
          var d = r.data_nascimento.split('T');         
          var df = d[0].split('-');
          r.data_nascimento = df[2] + "/" + df[1] + "/" + df[0];
          this.gestacao = r.gestacao;
     
          var d2 = new Date();
         
         var diff = moment(d2,"DD/MM/YYYY").diff(moment(r.data_nascimento,"DD/MM/YYYY"));
         var dias = moment.duration(diff).asDays();
         var mes = moment.duration(diff).asMonths();
         var ano = moment.duration(diff).asYears();
         this.idade = dias, mes, ano;

         if(Math.trunc(ano) == 0){
           if (Math.trunc(mes) == 0){
             if (Math.trunc(dias) <= 1){
               r.idade = Math.trunc(dias) + ' dia';
               this.idade = r.idade;
             }else {
               r.idade = Math.trunc(dias) + ' dias';
               this.idade = r.idade;
             }
             
           }else {
             if(Math.trunc(mes) <= 1){
               r.idade = Math.trunc(mes) + ' mês';
               this.idade = r.idade;
             }else {
               r.idade = Math.trunc(mes) + ' meses';
               this.idade = r.idade;
             }
             
           }
           
         }else {          
           if (Math.trunc(ano) == 1){
             if(Math.trunc(mes) == 12){
               r.idade = Math.trunc(ano) + ' ano';
               this.idade = r.idade;
             }else if(Math.trunc(mes) == 13){
               r.idade = Math.trunc(ano) + ' ano e 1 mês';
               this.idade = r.idade;
             }if(Math.trunc(mes) == 14){
               r.idade = Math.trunc(ano) + ' ano e 2 meses';
               this.idade = r.idade;
              }if(Math.trunc(mes) == 15){
               r.idade = Math.trunc(ano) + ' ano e 3 meses';
               this.idade = r.idade;
             }if(Math.trunc(mes) == 16){
               r.idade = Math.trunc(ano) + ' ano e 4 meses';
               this.idade = r.idade;
             }if(Math.trunc(mes) == 17){
               r.idade = Math.trunc(ano) + ' ano e 5 meses';
               this.idade = r.idade;
             }if(Math.trunc(mes) == 18){
               r.idade = Math.trunc(ano) + ' ano e 6 meses';
               this.idade = r.idade;
             }if(Math.trunc(mes) == 19){
               r.idade = Math.trunc(ano) + ' ano e 7 meses';
               this.idade = r.idade;
             }if(Math.trunc(mes) == 20){
               r.idade = Math.trunc(ano) + ' ano e 8 meses';
               this.idade = r.idade;
             }if(Math.trunc(mes) == 21){
               r.idade = Math.trunc(ano) + ' ano e 9 meses';
               this.idade = r.idade;
             }if(Math.trunc(mes) == 22){
               r.idade = Math.trunc(ano) + ' ano e 10 meses';
               this.idade = r.idade;
             }if(Math.trunc(mes) == 23){
               r.idade = Math.trunc(ano) + ' ano e 11 meses';
               this.idade = r.idade;
             }if(Math.trunc(mes) == 24){
               r.idade = Math.trunc(ano) + ' ano e 12 meses';
               this.idade = r.idade;
             }
           }
          else {
             r.idade = Math.trunc(ano) + ' anos';
             this.idade = r.idade;
           }
  
         }
        if(this.idade == '1 dia' || this.idade == '2 dias' || this.idade == '3 dias' || this.idade == '4 dias' || this.idade == '5 dias' || this.idade == '6 dias' || this.idade == '7 dias'){
          var message = 'Um bebê chegou! Vamos iniciar a vacinação!';
          this.notification7dias(message);
        } 
        if(this.idade == '2 meses' || this.idade == '4 meses' || this.idade == '6 meses' ){
          var message = 'Importante: muitas vacinas esse mês!!';
          this.notification246(message);
        } 
        if(this.idade == '3 meses' || this.idade == '5 meses'){
          var message = 'Vacina contra meningite! Fique atento.';
          this.notification35(message);
        } 
         if(this.idade == '6 meses'){
           var message = '“Bora” proteger contra a gripe? Vacina Influenza!';
           this.notification6(message);
         } 
         if(this.idade == '7 meses'){
          var message = 'Sentar sem apoio: um marco importante do desenvolvimento!';
          this.notification7(message);
        } 
        if(this.idade == '9 meses'){
          var message = 'Chegou a hora da vacina da Febre Amarela!';
          this.notification9(message);
        } 
        if(this.idade == '1 ano'){
          var message = 'Parabéns! Tem vacina nova e reforços te esperando!';
          this.notification1(message);
        } 
        if(this.idade == '1 ano e 3 meses'){
          var message = 'Tem vacinas! Meta: Imunidade em dia!';
          this.notification13(message);
        } 
        if(this.idade == '1 ano e 9 meses'){
          var message = 'Vai fazer a segunda dose da Hepatite A?';
          this.notification19(message);
        } 
        if(this.idade == '4 anos'){
          var message = 'Chegando reforço: Dia de vacinação!';
          this.notification4(message);
        } 
        if(this.idade == '9 anos'){
          var message = 'Já é hora da prevenção contra o HPV!';
          this.notification9A(message);
        } 

        if(this.idade == '11 anos' || this.idade == '12 anos' || this.idade == '13 anos' || this.idade == '14 anos' || this.idade == '15 anos' || this.idade == '16 anos' || this.idade == '17 anos' || this.idade == '18 anos' || this.idade == '19 anos' ){
          //1 x ao ano até 19 anos
          var message = 'Adolescente também tem que vacinar!';
          this.notification11(message);
        } 

        if(this.idade == '1 ano'){
          //APÓS 1 ANO – DE 6/6 MESES
          var message = 'Tomou “remédio para Verminose”?';
          this.notification1A(message);
        } 
        if(this.idade == '7 dias' || this.idade == '3 meses' || this.idade == '6 meses' || this.idade == '9 meses' || this.idade == '1 ano' || this.idade == '1 ano e 3 meses' ||  this.idade == '1 ano e 6 meses' || this.idade == '1 ano e 9 meses' || this.idade == '2 anos'){
          //A CADA 3 MESES: DO NASCIMENTO AOS 2 ANOS DE IDADE
          var message = 'Fique atento ao desenvolvimento da criança!';
          this.notification2A(message);
        } 
        if(this.idade == '3 meses'){
          var message = 'Visite seu Pediatra regularmente!';
          this.notification3(message);
        } 
        if(this.gestacao == 'prematuro'){
          if(this.idade == '15 dias' || this.idade == '1 mês' || this.idade == '2 meses' || this.idade == '3 meses' || this.idade == '4 meses' || this.idade == '5 meses' || this.idade == '6 meses' || this.idade == '7 meses' || this.idade == '8 meses' || this.idade == '9 meses' || this.idade == '10 meses' || this.idade == '11 meses' || this.idade == '1 ano' || this.idade == ' 1 anos e 1 mês' || this.idade == ' 1 anos e 2 meses' || this.idade == ' 1 anos e 3 meses' || this.idade == ' 1 anos e 4 meses' || this.idade == ' 1 anos e 5 meses' || this.idade == ' 1 anos e 6 meses' || this.idade == ' 1 anos e 7 meses' || this.idade == ' 1 anos e 8 meses' || this.idade == ' 1 anos e 9 meses' || this.idade == ' 1 anos e 10 meses' || this.idade == ' 1 anos e 11 meses'){
            var message = 'Prematuros: chegou a hora da Palivizumabe!';
            this.notificationp(message);
          }
        }

        })
        
      })

      
 
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  async toComparacao(tipo: any){
    const modal = await this.modalCtrl.create({
      component: ComparacaoVacinaPage,
      componentProps: {
        vacina: tipo
      }
    });
    return await modal.present();
  }

  notificationp(message){
    this.localNotifications.schedule({
      id:14,
      title:'',
      text: message,
      data: {page:'minha mensagem é essa'},
      launch:true,
      lockscreen: true,
      vibrate : true,
      trigger:{every:ELocalNotificationTriggerUnit.HOUR},
      led: true,
      silent: false,
      sound: '/assets/music/not.mp3',
      wakeup:true,
      foreground:true
    })

  }
  notification3(message){
    this.localNotifications.schedule({
      id:14,
      title:'',
      text: message,
      data: {page:'minha mensagem é essa'},
      launch:true,
      lockscreen: true,
      vibrate : true,
      trigger:{every:ELocalNotificationTriggerUnit.HOUR},
      led: true,
      silent: false,
      sound: '/assets/music/not.mp3',
      wakeup:true,
      foreground:true
    })

  }
  notification2A(message){
    this.localNotifications.schedule({
      id:15,
      title:'',
      text: message,
      data: {page:'minha mensagem é essa'},
      trigger:{every:ELocalNotificationTriggerUnit.HOUR},
      foreground:true
    })

  }
  notification1A(message){
    this.localNotifications.schedule({
      id:1,
      title:'',
      text: message,
      data: {page:'minha mensagem é essa'},
      launch:true,
      lockscreen: true,
      vibrate : true,
      trigger:{every:ELocalNotificationTriggerUnit.HOUR},
      led: true,
      silent: false,
      sound: '/assets/music/not.mp3',
      wakeup:true,
      foreground:true
    })

  }
  notification11(message){
    this.localNotifications.schedule({
      id:2,
      title:'',
      text: message,
      data: {page:'minha mensagem é essa'},
      launch:true,
      lockscreen: true,
      vibrate : true,
      trigger:{every:ELocalNotificationTriggerUnit.HOUR},
      led: true,
      silent: false,
      sound: '/assets/music/not.mp3',
      wakeup:true,
      foreground:true
    })

  }
  notification9A(message){
    this.localNotifications.schedule({
      id:3,
      title:'',
      text: message,
      data: {page:'minha mensagem é essa'},
      launch:true,
      lockscreen: true,
      vibrate : true,
      trigger:{every:ELocalNotificationTriggerUnit.HOUR},
      led: true,
      silent: false,
      sound: '/assets/music/not.mp3',
      wakeup:true,
      foreground:true
    })

  }
  notification4(message){
    this.localNotifications.schedule({
      id:4,
      title:'',
      text: message,
      data: {page:'minha mensagem é essa'},
      launch:true,
      lockscreen: true,
      vibrate : true,
      trigger:{every:ELocalNotificationTriggerUnit.HOUR},
      led: true,
      silent: false,
      sound: '/assets/music/not.mp3',
      wakeup:true,
      foreground:true
    })

  }
  notification19(message){
    this.localNotifications.schedule({
      id:5,
      title:'',
      text: message,
      data: {page:'minha mensagem é essa'},
      launch:true,
      lockscreen: true,
      vibrate : true,
      trigger:{every:ELocalNotificationTriggerUnit.HOUR},
      led: true,
      silent: false,
      sound: '/assets/music/not.mp3',
      wakeup:true,
      foreground:true
    })

  }
    notification7dias(message){
      this.localNotifications.schedule({
        id:6,
        title:'',
        text: message,
        data: {page:'minha mensagem é essa'},
        launch:true,
        lockscreen: true,
        vibrate : true,
        trigger:{every:ELocalNotificationTriggerUnit.HOUR},
        led: true,
        silent: false,
        sound: '/assets/music/not.mp3',
        wakeup:true,
        foreground:true
      })

    }
    notification246(message){
      this.localNotifications.schedule({
        id:7,
        title:'',
        text: message,
        data: {page:'minha mensagem é essa'},
        launch:true,
        lockscreen: true,
        vibrate : true,
        trigger:{every:ELocalNotificationTriggerUnit.HOUR},
        led: true,
        silent: false,
        sound: '/assets/music/not.mp3',
        wakeup:true,
        foreground:true
      })

    }
    notification35(message){
      this.localNotifications.schedule({
        id:8,
        title:'',
        text: message,
        data: {page:'minha mensagem é essa'},
        launch:true,
        lockscreen: true,
        vibrate : true,
        trigger:{every:ELocalNotificationTriggerUnit.HOUR},
        led: true,
        silent: false,
        sound: '/assets/music/not.mp3',
        wakeup:true,
        foreground:true
      })

    }
    notification6(message){
      this.localNotifications.schedule({
        id:9,
        title:'',
        text: message,
        data: {page:'minha mensagem é essa'},
        launch:true,
        lockscreen: true,
        vibrate : true,
        trigger:{every:ELocalNotificationTriggerUnit.HOUR},
        led: true,
        silent: false,
        sound: '/assets/music/not.mp3',
        wakeup:true,
        foreground:true
      })

    }
    notification7(message){
      this.localNotifications.schedule({
        id:10,
        title:'',
        text: message,
        data: {page:'minha mensagem é essa'},
        launch:true,
        lockscreen: true,
        vibrate : true,
        trigger:{every:ELocalNotificationTriggerUnit.HOUR},
        led: true,
        silent: false,
        sound: '/assets/music/not.mp3',
        wakeup:true,
        foreground:true
      })

    }
    notification9(message){
      this.localNotifications.schedule({
        id:11,
        title:'',
        text: message,
        data: {page:'minha mensagem é essa'},
        launch:true,
        lockscreen: true,
        vibrate : true,
        trigger:{every:ELocalNotificationTriggerUnit.HOUR},
        led: true,
        silent: false,
        sound: '/assets/music/not.mp3',
        wakeup:true,
        foreground:true
      })

    }
    notification1(message){
      this.localNotifications.schedule({
        id:12,
        title:'',
        text: message,
        data: {page:'minha mensagem é essa'},
        launch:true,
        lockscreen: true,
        vibrate : true,
        trigger:{every:ELocalNotificationTriggerUnit.HOUR},
        led: true,
        silent: false,
        sound: '/assets/music/not.mp3',
        wakeup:true,
        foreground:true
      })

    }
    notification13(message){
      this.localNotifications.schedule({
        id:13,
        title:'',
        text: message,
        data: {page:'minha mensagem é essa'},
        launch:true,
        lockscreen: true,
        vibrate : true,
        trigger:{every:ELocalNotificationTriggerUnit.HOUR},
        led: true,
        silent: false,
        sound: 'file://assets/music/not.mp3',
        wakeup:true,
        foreground:true
      })

    }
  repeatingDaily(){
    
    this.localNotifications.schedule({
      id:3,
      title:'DAILY',
      text:'DEUUU CERTOOOO',
      data: {page:'minha mensagem é essa'},
      trigger:{every:{hour:14,minute:25}},
      foreground:true
    })
  }

  getAll(){
    this.localNotifications.getAll().then(res => {
      this.schedule = res;
    })

  }

  showAlert(header,sub,msg){
    this.alertCrtl.create({
      header:header,
      subHeader:sub,
      message:msg,
      buttons:['OK']
    }).then(alert =>alert.present());
  }

  finalizarVacina(rn) {
    var d = new Date();
    var data = moment(d).format('YYYY-MM-DD');


  }

  async vacinasTomar(idade,rn) {
    
    const modal = await this.modalCtrl.create({
      component: VacinaTomarPage,
      componentProps: {
        idade: idade,
        rn:rn
      }
    });

    return await modal.present();

  }
}
