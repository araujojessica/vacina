import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadernetaPage } from './caderneta.page';

const routes: Routes = [
  {
    path: '',
    component: CadernetaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadernetaPageRoutingModule {}
