import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadernetaPageRoutingModule } from './caderneta-routing.module';

import { CadernetaPage } from './caderneta.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadernetaPageRoutingModule
  ],
  declarations: [CadernetaPage]
})
export class CadernetaPageModule {}
