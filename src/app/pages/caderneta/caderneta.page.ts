import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController, NavParams, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { NewRN } from 'src/app/interfaces/newRN';
import { RegisterService } from 'src/app/services/register/register';
import { ActivatedRoute, Router } from '@angular/router';
import polling from 'rx-polling';

@Component({
  selector: 'app-caderneta',
  templateUrl: './caderneta.page.html',
  styleUrls: ['./caderneta.page.scss'],
})
export class CadernetaPage implements OnInit {
  user$: Observable<User>;
  nome: string
  idResponsavel:any;
  nomeRn:any;
  rn:any[];
  caderneta: any[];
  idRn:any;
  constructor(
    public modalCtrl: ModalController,
    private registerService:RegisterService,
    private userService: UserService,
    private route: ActivatedRoute,
    private params: NavParams,
    private toastCtrl: ToastController,
    private menuCtrl:MenuController

  ) { 

     this.idRn = this.params.get('rn');
     this.nomeRn = this.params.get('nome');
    
    this.user$ = userService.getUser();
    this.user$.subscribe( usuario => {
     var n = usuario.fullName.split(' ');
     this.nome = n[0];  
     this.idResponsavel = usuario.id;   
    });

    const options = { interval: 15000 };
    const r = this.registerService.findCheck(this.idRn);
    polling(r,options).subscribe(caderneta =>{   
      this.caderneta = caderneta;
      this.caderneta.forEach(c => {

        var d = c.data.split(' ');
        var d1 = d[0].split('-');
        c.data = d1[2] + ' / ' + d1[1] + ' / ' + d1[0];

        var vac = c.vacina.split(',');
        c.vacina = vac[0];
        if(vac[1] == 'sus'){
          c.user = 'SUS';
        }else {
          c.user = 'PARTICULAR';
        }
        
        
      })
      
    })
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  excluir(id) {
    this.registerService.deleteCheck(id).subscribe(
        async () => {
                
            const toast = await (await this.toastCtrl.create({
                  message: 'Vacinação excluída com sucesso!',
                  duration: 4000, position: 'middle',
                  color: 'success'
                })).present();

        },  
        async err => {
          console.log(err)
          const toast = await (await this.toastCtrl.create({
            message: 'Ocorreu um erro, por favor contate nosso ADM!',
            duration: 4000, position: 'middle',
            color: 'danger'
          })).present();
        }
    );
    
  }

  dismiss(){
    this.modalCtrl.dismiss();
  }
  async cad(rn){
    const modal = await this.modalCtrl.create({
      component: CadernetaPage,
      componentProps: {
        rn: rn
      }
    });
    return await modal.present();
  }

}
