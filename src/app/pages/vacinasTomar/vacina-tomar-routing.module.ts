import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VacinaTomarPage } from './vacina-tomar.page';

const routes: Routes = [
  {
    path: '',
    component: VacinaTomarPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VacinaTomarPageRoutingModule {}
