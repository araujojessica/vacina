import { ThrowStmt } from '@angular/compiler';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ELocalNotificationTriggerUnit, LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { AlertController, MenuController, ModalController, NavParams, Platform, ToastController } from '@ionic/angular';
import { Console } from 'console';
import * as moment from 'moment';
import polling from 'rx-polling';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { NewRN } from 'src/app/interfaces/newRN';
import { Vacina } from 'src/app/interfaces/vacina';
import { RegisterService } from 'src/app/services/register/register';
import { runInThisContext } from 'vm';
import { CompPage } from '../comp/comp.page';
import { ComparacaoVacinaPage } from '../comparacao-vacina/comparacao-vacina.page';

@Component({
  selector: 'app-vacina-tomar',
  templateUrl: 'vacina-tomar.page.html',
  styleUrls: ['vacina-tomar.page.scss'],
})
export class VacinaTomarPage {

  nome: string
  idResponsavel:any;
  schedule=[];
  DataArray: Array<string> = [];
  user$: Observable<User>;
  rn:NewRN[];
  idade:any;
  r:any;
  vacina: Vacina[];
  caderneta: any[];

  constructor(
    public router: Router,
    public menuCtrl: MenuController,
    public plt:Platform,
    public localNotifications:LocalNotifications,
    public alertCrtl: AlertController,
    public modalCtrl: ModalController,
    private userService: UserService,
    private registerService: RegisterService,
    private params: NavParams,
    private toastCtrl: ToastController

  ) {
    this.idade = this.params.get('idade');
    this.r = this.params.get('rn');
    this.user$ = userService.getUser();
    this.user$.subscribe( usuario => {
     var n = usuario.fullName.split(' ');
     this.nome = n[0];  
     this.idResponsavel = usuario.id;   
    });

    const options = { interval: 15000 };
    const r = this.registerService.findVacina(this.idade);
      polling(r,options).subscribe(vacina =>{
        this.vacina = vacina;
        this.vacina.forEach(v => {
          this.registerService.findCheck(this.r).subscribe(caderneta => {
            this.caderneta = caderneta;    
           
             this.caderneta.forEach(c=> {
              
              var d = c.vacina.split(',');
              c.vacina = d[0];
              c.data = d[1];
              c.user = d[2];
              if(c.vacina == 'PENTA + VIP' || c.vacina == 'HEXA' || c.vacina == 'DPT + VOP' || c.vacina == 'PENTA ACELULAR') {
                c.vacina = 'PENTA';
              }
              if(c.vacina == 'MENINGOCÓCICA C' || c.vacina == 'MENINGOCÓCICA ACWY') {
                c.vacina = 'MENINGO';
              }
              if(c.id == '39') {
                if(c.vacina == 'DPT + VOP' || c.vacina == 'DTPa + VIP') {
                  c.vacina = 'MENINGO';
                }
              }
              if(c.id == '24') {
                if(c.vacina == 'MENINGOCÓCICA ACWY') {
                  c.vacina = 'MENINGOCÓCICA ACWY';
                }
              }
              if(v.nome == c.vacina){
                v.cadastro = 'nao';
              }
            })
      
          })
        })
        
      })
}

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  async toComparacao(tipo){
    const modal = await this.modalCtrl.create({
      component: ComparacaoVacinaPage,
      componentProps: {
        vacina: tipo
      }
    });
    return await modal.present();
  }

  finalizarVacina(nome){
    var d = new Date();
    var data = moment(d).format('YYYY-MM-DD');
    this.registerService.registerCheck(this.idResponsavel,this.r,nome,data).subscribe(

      async () => {
            
        const toast = await (await this.toastCtrl.create({
              message: 'Vacina registrada com Sucesso',
              duration: 4000, position: 'middle',
              color: 'success'
            })).present();

     },  
    async err => {
      console.log(err)
      const toast = await (await this.toastCtrl.create({
        message: 'Ocorreu um erro, por favor contate nosso ADM!',
        duration: 4000, position: 'middle',
        color: 'danger'
      })).present();
    }

    )
  }
  gotohome(){
    this.router.navigate(['/']);
  }

  dismiss(){
    this.modalCtrl.dismiss();
  }

  check(vacina,tipo){
    var v  = vacina + ',' + tipo + ',' + this.idade;
    var data = new Date();
    var month = data.getMonth()+1;
    var d = data.getDate() + " / " + month + " / " + data.getFullYear();
  
    var d1 =  data.getFullYear() + "-" + month + "-" + data.getDate() + " " + data.getHours() + ":"+data.getMinutes();

   this.registerService.registerCheck(this.idResponsavel,this.rn,v,d1).subscribe(

      async () => {
            
        const toast = await (await this.toastCtrl.create({
              message: 'Tomou a vacina em  : '+d+'',
              duration: 4000, position: 'middle',
              color: 'success'
            })).present();

     },  
    async err => {
      console.log(err)
      const toast = await (await this.toastCtrl.create({
        message: 'Ocorreu um erro, por favor contate nosso ADM!',
        duration: 4000, position: 'middle',
        color: 'danger'
      })).present();
    }

    );

  }

  
  async comparacao(nome: any){

    if(nome == 'ROTA VÍRUS') {
      nome = 'ROTAVÍRUS';
    }
    if(nome == 'PNEUMOCÓCICA') {
      nome = 'PNEUMO'
    }
    if(nome == 'MENINGOCÓCICA B') {
      nome = 'MENINGO B'
    }
    const modal = await this.modalCtrl.create({
      component: CompPage,
      componentProps: {
        nome: nome
      }
    });
    return await modal.present();

  }
  
}
 
