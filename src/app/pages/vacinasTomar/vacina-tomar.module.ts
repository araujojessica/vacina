import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { VacinaTomarPage } from './vacina-tomar.page';

import {VacinaTomarPageRoutingModule } from './vacina-tomar-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    
    VacinaTomarPageRoutingModule
  ],
  declarations: [VacinaTomarPage]
})
export class VacinaTomarPageModule {}
