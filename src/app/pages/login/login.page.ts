import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, MenuController, NavController, ToastController } from '@ionic/angular';
import { AuthService } from 'src/app/core/auth/auth.service';
import { RecuperarSenhaService } from 'src/app/services/register/recuperar_senha';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public Login: FormGroup;
  nome : string; 
  tipo: boolean;

  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    public menuCtrl: MenuController,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private authService: AuthService,
    private recuperarSenha: RecuperarSenhaService
  ) {

   }

  ngOnInit() {
    this.Login = this.formBuilder.group({
      nome: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required,Validators.minLength(1),Validators.maxLength(14)])],
    });
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  toHome(){
    localStorage.setItem('nome', this.Login.get('nome').value);
    var sus = this.Login.get('nome').value;
    var password = this.Login.get('password').value;
  
    this.authService.authenticate(sus, password).subscribe( 
      () => this.navCtrl.navigateRoot('/cadastro'),
        async err => {
             
            //this.onLoginForm.reset();
            
            const toast = await this.toastCtrl.create({
                    message:'Por favor verifique o número da sua carteirinha do sus ou registre-se me nosso app!', 
                    duration:4000, position:'top',
                    color:'danger'
            });               
           toast.present();
  
          }
        
    ); 
  }
  mostrarSenha(){
    this.tipo = !this.tipo;
  }

  toSignup(){
    this.router.navigate(['/signup']);
  }

  async forgotPassword(){
    const alert = await this.alertCtrl.create({
      header: 'Esqueceu sua senha?',
      message: 'Informe seu e-mail para enviarmos um novo link de recuperação.',
      inputs: [
        {
          name: 'email',
          type: 'email',
          placeholder: 'E-mail'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirmar Cancelamento?');
          }
        }, {
          text: 'Confirmar',
          handler: async data => {
            console.log('rec : ' + data.email)
            this.recuperarSenha.sendemail(data.email)
            .subscribe(
                async () =>{ 
                  this.navCtrl.navigateRoot('')
                  const toast = await this.toastCtrl.create({
                            
                    message:'Email enviado com sucesso!', 
                    duration:4000, position:'middle',
                    color:'success'
                    });
            
                  
                  toast.present();
                },
                async err => {
             
                   const toast = await this.toastCtrl.create({
                            
                            message:'Usuário não cadastrado, por favor cadastre-se no APP!', 
                            duration:4000, position:'middle',
                            color:'danger'
                    });
            
                   
                   toast.present();
    
                }
                
            );
            
          }
        }
      ]
    });

    await alert.present();
  }
}
