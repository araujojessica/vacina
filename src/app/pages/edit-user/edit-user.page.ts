import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MenuController, ModalController, NavParams, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { NewUser } from 'src/app/interfaces/newUser';
import { RegisterService } from 'src/app/services/register/register';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.page.html',
  styleUrls: ['./edit-user.page.scss'],
})
export class EditUserPage implements OnInit {
  user$: Observable<User>;
  fullName: any;
  sus:any;
  nome:any;
  newNome:any;
  username:any;
  idUsuario:any;
  email:any;
  usuario: NewUser;
  s:any;
  public EditForm: FormGroup;
  constructor(
    public modalCtrl: ModalController,
   // public params: NavParams,
    public route: ActivatedRoute,
    public registerService: RegisterService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private menuCtrl: MenuController
  ) {

    this.user$ = userService.getUser();
    this.user$.subscribe( usuario => {

     this.s = usuario.sus;
     
     

    });

    this.registerService.findUserUnico(this.s).subscribe(usuario => {

      this.fullName = usuario.user_full_name;
      this.sus = usuario.user_sus;
      this.idUsuario = usuario.user_id;
      this.email = usuario.user_email;
      
    })

   }

  ngOnInit() {
    this.EditForm = this.formBuilder.group({
      fullName: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required,Validators.minLength(1),Validators.maxLength(14)])],
      email: [null, Validators.compose([Validators.required,Validators.email])]
    });
  }


  mudarNome(){
    const newUser  = this.EditForm.getRawValue() as NewUser;
    newUser.id = this.idUsuario;
    
    if(newUser.password != undefined) {
      this.registerService.editUsuarioPassword(newUser).subscribe( 
        async () => {
              
          const toast = await (await this.toastCtrl.create({
                message: 'Usuário editado com sucesso!',
                duration: 4000, position: 'middle',
                color: 'success'
              })).present();
  
       },  
        async err => {
          console.log(err)
          const toast = await (await this.toastCtrl.create({
            message: 'Ocorreu um erro, por favor contate nosso ADM!',
            duration: 4000, position: 'middle',
            color: 'danger'
          })).present();
      
        })
    }
    if(newUser.fullName != undefined){
    this.registerService.editUsuario(newUser).subscribe( 
      async () => {
            
        const toast = await (await this.toastCtrl.create({
              message: 'Usuário editado com sucesso!',
              duration: 4000, position: 'middle',
              color: 'success'
            })).present();

     },  
      async err => {
        console.log(err)
        const toast = await (await this.toastCtrl.create({
          message: 'Ocorreu um erro, por favor contate nosso ADM!',
          duration: 4000, position: 'middle',
          color: 'danger'
        })).present();
    
      })
    }

    if(newUser.email != undefined){
      this.registerService.editUsuarioEmail(newUser).subscribe( 
        async () => {
              
          const toast = await (await this.toastCtrl.create({
                message: 'Usuário editado com sucesso!',
                duration: 4000, position: 'middle',
                color: 'success'
              })).present();
  
       },  
        async err => {
          console.log(err)
          const toast = await (await this.toastCtrl.create({
            message: 'Ocorreu um erro, por favor contate nosso ADM!',
            duration: 4000, position: 'middle',
            color: 'danger'
          })).present();
      
        })
      }
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  dismiss(){
    this.modalCtrl.dismiss();
  }

}
