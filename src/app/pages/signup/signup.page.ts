import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { NewUser } from 'src/app/interfaces/newUser';
import { RegisterService } from 'src/app/services/register/register';
import { UserNotTakenValidatorService } from 'src/app/services/register/user-not-taken.validator.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  public Login: FormGroup;
  tipo: boolean;
  
  constructor(
    private formBuilder: FormBuilder,
    private userNotTakenValidatorService: UserNotTakenValidatorService,
    private toastCtrl: ToastController,
    private signupService: RegisterService,
    private router: Router,
    
  ) { }

  ngOnInit() {
    this.Login = this.formBuilder.group({
      fullName: [null, Validators.compose([Validators.required])],
      sus: [null,[Validators.required],this.userNotTakenValidatorService.checkUserNameTaken()],
      password: [null, Validators.compose([Validators.required,Validators.minLength(1),Validators.maxLength(14)])],
      email:[null, [Validators.required, Validators.email],this.userNotTakenValidatorService.checkEmailTaken()]
    });
  }

  async signup() {
      const newUser  = this.Login.getRawValue() as NewUser;
       console.log(newUser.sus)
      this.signupService.signup(newUser).subscribe(
        
        async () => {
          
            const toast = await (await this.toastCtrl.create({
                  message: 'Usuário cadastrado com sucesso! Agora é só fazer o login!',
                  duration: 4000, position: 'middle',
                  color: 'success'
                })).present();

          this.router.navigate(['/'])
          
        
        },  
        async err => {
          console.log(err)
          const toast = await (await this.toastCtrl.create({
            message: 'Ocorreu um erro, por favor contate nosso ADM!',
            duration: 4000, position: 'middle',
            color: 'danger'
          })).present();
        }
      );

}
mostrarSenha(){
  console.log(`entrou`)
  this.tipo = !this.tipo;
}

}
