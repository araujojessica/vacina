import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {map, tap} from 'rxjs/operators';
import { UserService } from '../user/user.service';


const API_URL = 'http://159.203.181.9:3005';
//const API_URL = 'http://localhost:3005';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http : HttpClient, private userService: UserService) { }

  authenticate( userName:string, password:string) {
    return this.http.post(`${API_URL}/user/login`,{userName,password},{observe:'response'})
      .pipe(tap(res => {
        const authToken = res.headers.get('x-access-token');
        this.userService.setToken(authToken);
        //console.log(`User ${userName} authenticated with token ${authToken}`)
      }))
      
  }

  findAllUsers() {

    return this.http.get(`${API_URL}/user/findAllUsers`,{observe:'response'})
    .pipe(tap(res => {

      //console.log(`All users` + res)
    }))

  }

  finduser(email) {

    return this.http.get<any>(API_URL +  `/user/find/`+email+``);
   
  }


 
}